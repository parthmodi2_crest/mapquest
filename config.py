from dotenv import load_dotenv
import os

load_dotenv('.env')

class Config(object):
    API_TOKEN=os.environ.get('MAPQUEST_API_KEY')
