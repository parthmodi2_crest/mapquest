import logging
import requests
from requests.sessions import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

from config import Config
from custom_decorators import handle_request_exception
from mapquest_location import MapquestLocation

class Mapquest(object):
    BASE_URL = 'http://www.mapquestapi.com/geocoding/v1'
    FORMAT = 'json'
    DEFAULT_TIMEOUT = "3"
    DEFAULT_MAX_RESULTS = "1"
    DEFAULT_MAX_RETRIES = 10
    
    def __init__(self, timeout=None, max_results=None, max_retries=None):
        logging.basicConfig(level=logging.DEBUG)
        s = requests.Session()
        s.headers.update({ "timeout": str(timeout or self.DEFAULT_TIMEOUT) })
        s.params.update({"key": Config.API_TOKEN, "outFormat": self.FORMAT, "maxResults": str(max_results or self.DEFAULT_MAX_RESULTS)})
        retries = Retry(total=(max_retries or self.DEFAULT_MAX_RETRIES), backoff_factor=1, method_whitelist=['GET', 'POST'])
        s.mount("http://", HTTPAdapter(max_retries=retries))
        self._connection = s
    
    @handle_request_exception
    def get_address(self, location):
        url = self.BASE_URL + "/address"
        result = self._connection.get(url=url, params=self.build_fetch_address_payload(location))
        result.raise_for_status()
        return self.extract_locations_from_response(result.json())
        
    @handle_request_exception
    def post_address(self, location):
        url = self.BASE_URL + "/address"
        result = self._connection.post(url, json=self.build_fetch_address_payload(location))
        result.raise_for_status()
        return self.extract_locations_from_response(result.json())
    
    @handle_request_exception
    def get_reverse(self, location, include_road_metadata=True, include_nearest_intersection=True):
        url = self.BASE_URL + "/reverse"
        result = self._connection.get(
                url=url, 
            params={"location": location, "includeRoadMetadata": include_road_metadata, "includeNearestIntersection": include_nearest_intersection}
        )
        result.raise_for_status()
        return self.extract_locations_from_response(result.json())
    
    @handle_request_exception
    def post_reverse(self, location, include_road_metadata=True, include_nearest_intersection=True):
        url = self.BASE_URL + "/reverse"
        result = self._connection.post(
            url=url, 
            json={"location": self.extract_lat_lng(location), "includeRoadMetadata": include_road_metadata, "includeNearestIntersection": include_nearest_intersection}
        )
        result.raise_for_status()
        return self.extract_locations_from_response(result.json())
    
    @handle_request_exception
    def get_batch(self, locations):
        url = self.BASE_URL + "/batch"
        locations = list(filter(lambda location: location is not None and location.strip() != "", locations))
        location_lists = [locations[i:i+100] for i in range(0, len(locations), 100)]
    
        resp_list = []
        for location_list in location_lists:    
            resp = self._connection.get(url, params={"location": location_list})
            resp.raise_for_status()
            result_locations = self.extract_locations_from_response(resp.json())
            for result in result_locations:
                resp_list.append(result)
        return resp_list
    
            
    def extract_lat_lng(self, location):
        coordinates = location.split(",")
        msg = "Please enter valid coordinates in `lat,lng` format"
        if len(coordinates) != 2: raise Exception(msg)
        
        try:
            lat = float(coordinates[0].strip())
            lng = float(coordinates[1].strip())
            return { "latLng": { "lat": lat, "lng": lng } }
        except ValueError:
            raise Exception(msg)
    
    
    def build_fetch_address_payload(self, location):
        return {"location": location, "maxResults": self.max_results }
    
    
    def extract_locations_from_response(self, result_json):
        if result_json.get('info', {}).get('statuscode') != 0:
            return None
        res_arr = result_json['results']
        
        mq_locs = []
        for res in res_arr:
            for res_loc in res['locations']:
                mq_locs.append(MapquestLocation(res_loc))
                
        return mq_locs
