import functools
from requests.exceptions import RequestException
from json.decoder import JSONDecodeError

def handle_request_exception(func):
        @functools.wraps(func)
        def decorated_func(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except RequestException as re:
                print("Something went wrong: " + str(re))
            except JSONDecodeError:
                print("Request failed!")
        return decorated_func
