# {
#     "info": {
#         "statuscode": 0,
#         "copyright": {
#             "text": "© 2021 MapQuest, Inc.",
#             "imageUrl": "http://api.mqcdn.com/res/mqlogo.gif",
#             "imageAltText": "© 2021 MapQuest, Inc."
#         },
#         "messages": []
#     },
#     "options": {
#         "maxResults": 1,
#         "thumbMaps": True,
#         "ignoreLatLngInput": False
#     },
#     "results": [
#         {
#             "providedLocation": {
#                 "location": "Ahmedabad,GJ"
#             },
#             "locations": [
#                 {
#                     "street": "GJ-81",
#                     "adminArea6": "",
#                     "adminArea6Type": "Neighborhood",
#                     "adminArea5": "Gijón",
#                     "adminArea5Type": "City",
#                     "adminArea4": "",
#                     "adminArea4Type": "County",
#                     "adminArea3": "Principality of Asturias",
#                     "adminArea3Type": "State",
#                     "adminArea1": "ES",
#                     "adminArea1Type": "Country",
#                     "postalCode": "",
#                     "geocodeQualityCode": "B3CCA",
#                     "geocodeQuality": "STREET",
#                     "dragPoint": False,
#                     "sideOfStreet": "N",
#                     "linkId": "ES/STR/p0/1022154",
#                     "unknownInput": "",
#                     "type": "s",
#                     "latLng": {
#                         "lat": 43.52429,
#                         "lng": -5.69279
#                     },
#                     "displayLatLng": {
#                         "lat": 43.52429,
#                         "lng": -5.69279
#                     },
#                     "mapUrl": "http://www.mapquestapi.com/staticmap/v5/map?key=pF3RNzWrnykAToAp4zZD4wD1Vyuaey77&type=map&size=225,160&locations=43.52429,-5.69279|marker-sm-50318A-1&scalebar=true&zoom=15&rand=2067305532"
#                 }
#             ]
#         }
#     ]
# }
class MapquestLocation(object):
    def __init__(self, json_content):
        self.raw = json_content
        
    def __repr__(self):
        """ Display [address] if available; [lat,lng] otherwise"""
        return u'MapquestLocation<{0},{1}>'.format(self.lat, self.lng)
    
    @property
    def lat(self):
        return self.raw.get('latLng', {}).get('lat')
    
    @property
    def lng(self):
        return self.raw.get('latLng', {}).get('lng')
    
    @property
    def postal_code(self):
        return self.raw.get('postalCode')

    @property
    def quality(self):
        return self.raw.get('geocodeQuality')
        
    @property
    def city(self):
        return self.raw.get('adminArea5')
    
    @property
    def county(self):
        return self.raw.get('adminArea4')

    @property
    def state(self):
        return self.raw.get('adminArea3')

    @property
    def country(self):
        return self.raw.get('adminArea1')
    